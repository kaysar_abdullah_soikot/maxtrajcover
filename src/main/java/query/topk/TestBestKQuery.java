package query.topk;

import java.util.ArrayList;

import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import ds.qtrajtree.QuadTrajTree;
import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;
import query.QueryGraphGenerator;
import query.service.ServiceQueryProcessor;

public class TestBestKQuery {

    static int N = 64;
    static int K = 8;

    public static void run(QuadTrajTree quadTrajTree, ArrayList<CoordinateArraySequence> facilityGraph) {
        ArrayList<ArrayList<CoordinateArraySequence>> facilityQueries = new ArrayList<ArrayList<CoordinateArraySequence>>();

        int numberOfRuns = 100;
        double naiveTime = 0, zOrderTime = 0;
        for (int run = 0; run < numberOfRuns; run++) {
            facilityQueries.clear();
            for (int i = 0; i < N; i++) {
                facilityQueries.add(new ArrayList<CoordinateArraySequence>(QueryGraphGenerator.generateQuery(facilityGraph)));
            }
            if (facilityQueries.isEmpty()){
                run--;
                continue;
            }
            @SuppressWarnings("unchecked")
            ArrayList<ArrayList<CoordinateArraySequence>> tempFacilityQueries = (ArrayList<ArrayList<CoordinateArraySequence>>) facilityQueries.clone();
            BestKQueryProcessor processQuery = new BestKQueryProcessor(quadTrajTree);
            
            double from = System.nanoTime();
            ArrayList<CandidateSolution> candidateSolutions = processQuery.bestKFacilities(facilityQueries, K);
            double to = System.nanoTime();
            /*
            for (CandidateSolution candidateSolution : candidateSolutions) {
                System.out.println(candidateSolution.id + "-->" + candidateSolution.fitness());
            }
            System.out.println("Time for best K: " + (to - from) / 1e9 + "s");
            */
            
            //System.out.println("\nBruteforce:\n");
            
            zOrderTime += (to - from) / 1e9;
            /*
            int id = 0;
            ServiceQueryProcessor serviceQueryProcessor = new ServiceQueryProcessor(quadTrajTree);
            facilityServiceComparator comparator = new facilityServiceComparator();
            PriorityQueue<serviceByFacilities> facilityService = new PriorityQueue(facilityQueries.size(), comparator);
            from = System.nanoTime();
            for (ArrayList<CoordinateArraySequence> facilityQuery : facilityQueries) {
                HashSet<Integer> served = new HashSet<Integer>();
                double serviceValue = serviceQueryProcessor.evaluateService(quadTrajTree.getQuadTree().getRootNode(), facilityQuery, served);
                facilityService.add(new serviceByFacilities((id++), (int) serviceValue));
            }
            to = System.nanoTime();
            
            for (int i = 0; i < K; i++) {
                serviceByFacilities temp = facilityService.poll();
                System.out.println(temp.id + "-->" + temp.service);
            }
            System.out.println("Time for Brute Force best K: " + (to - from) / 1e9 + "s");
            
            naiveTime += (to - from) / 1e9;
            */
            
        }
        //naiveTime /= numberOfRuns;
        zOrderTime /= numberOfRuns;
        System.out.println (zOrderTime);
    }
}
