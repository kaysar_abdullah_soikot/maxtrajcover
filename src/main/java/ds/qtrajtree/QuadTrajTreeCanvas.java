package ds.qtrajtree;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JComponent;
import javax.swing.JFrame;

import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import ds.qtree.Node;


public class QuadTrajTreeCanvas extends JComponent{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4703035325723368366L;
	QuadTrajTree quadTrajTree;
	public QuadTrajTreeCanvas(QuadTrajTree quadTrajTree) {
		super();
		this.quadTrajTree = quadTrajTree;
	}

	public void draw() {
		JFrame window = new JFrame();
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setBounds(0, 0, 510, 510);
		window.getContentPane().add(this);
		window.setVisible(true);
    }
	
	public void paint(Graphics g) {
		//int count = 0;
		for (Entry<Node, ArrayList<CoordinateArraySequence>> entry : quadTrajTree.nodeToIntraTrajsMap.entrySet())
		{
			Node node = entry.getKey();
			Color color = new Color((int)(Math.random() * 0x1000000));
			g.setColor(color);
			g.drawRect ((int)node.getX()*5, (int)node.getY()*5, (int)node.getW()*5, (int)node.getH()*5);
			ArrayList<CoordinateArraySequence> trajectories 
			= entry.getValue();
			for (CoordinateArraySequence trajectory : trajectories) {
				g.drawLine((int)trajectory.getX(0)*5, (int)trajectory.getY(0)*5, 
						(int)trajectory.getX(1)*5, (int)trajectory.getY(1)*5);
				//count++;
			}
			
		}
		//System.out.println(count);
	}
}