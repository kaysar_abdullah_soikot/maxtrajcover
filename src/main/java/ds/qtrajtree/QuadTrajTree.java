package ds.qtrajtree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.javatuples.Pair;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.util.Assert;

import ds.qtree.Node;
import ds.qtree.Point;
import ds.qtree.QuadTree;

public class QuadTrajTree {

    final double proximity;
    private QuadTree quadTree;

    // a trajectory belongs to which node
    //public Map<String, String> trajToNodeStrMap = new HashMap<String, String>();
    //public Map<CoordinateArraySequence, Node> intraTrajToNodeMap = new HashMap<CoordinateArraySequence, Node>();
    public Map<Node, ArrayList<CoordinateArraySequence>> nodeToIntraTrajsMap = new HashMap<Node, ArrayList<CoordinateArraySequence>>();
    public Map<Node, Integer> nodeToAllTrajsCount = new HashMap<Node, Integer>();
    public Map<Node, ArrayList<Integer>> nodeToIntraTrajsIdMap = new HashMap<Node, ArrayList<Integer>>();
    public Map<Node, QuadTree> nodeToIntraTrajQuadTree = new HashMap<Node, QuadTree>();

    public QuadTrajTree(ArrayList<CoordinateArraySequence> trajectories) {
        proximity = 0.05;
        Envelope envelope = new Envelope();
        for (CoordinateArraySequence trajectory : trajectories) {
            trajectory.expandEnvelope(envelope);
        }

        quadTree = new QuadTree(envelope.getMinX(), envelope.getMinY(),
                envelope.getMaxX(), envelope.getMaxY());

        for (CoordinateArraySequence trajectory : trajectories) {
            for (int i = 0; i < trajectory.size(); i++) {
                quadTree.set(trajectory.getX(i), trajectory.getY(i), new Integer(i));
            }
        }
        addTrajectories(trajectories);
        System.out.println("Tree built on trajectory data set of size: " + trajectories.size());
    }

    private void addTrajectories(ArrayList<CoordinateArraySequence> trajectories) {
        int trajID = 0;
        for (CoordinateArraySequence trajectory : trajectories) {
            Node node = addTrajectory(quadTree.getRootNode(), trajectory);

            //System.out.println(trajectory.toString() + node.toString());
            //intraTrajToNodeMap.put(trajectory, node);

            if (!nodeToIntraTrajsMap.containsKey(node)) {
                nodeToIntraTrajsMap.put(node, new ArrayList<CoordinateArraySequence>());
                nodeToIntraTrajsIdMap.put(node, new ArrayList<Integer>());
                nodeToIntraTrajQuadTree.put(node, new QuadTree(node.getX(), node.getY(), node.getX() + node.getW(), node.getY() + node.getH()));
            }
            nodeToIntraTrajsMap.get(node).add(trajectory);
            //Integer t_id = trajID;
            nodeToIntraTrajsIdMap.get(node).add(trajID);

            nodeToIntraTrajQuadTree.get(node).set(trajectory.getX(0), trajectory.getY(0), new Pair<Integer, Boolean>(new Integer(trajID), new Boolean(true)));
            nodeToIntraTrajQuadTree.get(node).set(trajectory.getX(1), trajectory.getY(1), new Pair<Integer, Boolean>(new Integer(trajID), new Boolean(false)));

            //System.out.println(trajID + ": <" + trajectory.getX(0) + ", " + trajectory.getY(0) + ">        <" + trajectory.getX(1) + ", " + trajectory.getY(1) + ">");
            trajID++;
        }
    }

    private Node addTrajectory(Node node, CoordinateArraySequence trajectory) {

        //System.out.println(node);
        if (nodeToAllTrajsCount.get(node) == null) {
            nodeToAllTrajsCount.put(node, 0);
        }
        nodeToAllTrajsCount.put(node, nodeToAllTrajsCount.get(node) + 1);

        Envelope trajEnv = new Envelope();

        for (int i = 0; i < trajectory.size(); i++) {
            trajEnv.expandToInclude(trajectory.getCoordinate(i));
        }
        Envelope envNe = getNodeEnvelop(node.getNe());
        if (!envNe.contains(trajEnv) && envNe.intersects(trajEnv)) {
            return node;
        }
        Envelope envNw = getNodeEnvelop(node.getNw());
        if (!envNw.contains(trajEnv) && envNw.intersects(trajEnv)) {
            return node;
        }
        Envelope envSe = getNodeEnvelop(node.getSe());
        if (!envSe.contains(trajEnv) && envSe.intersects(trajEnv)) {
            return node;
        }
        Envelope envSw = getNodeEnvelop(node.getSw());
        if (!envSw.contains(trajEnv) && envSw.intersects(trajEnv)) {
            return node;
        }

        if (envNe.contains(trajEnv)) {
            return addTrajectory(node.getNe(), trajectory);
        }
        if (envNw.contains(trajEnv)) {
            return addTrajectory(node.getNw(), trajectory);
        }
        if (envSe.contains(trajEnv)) {
            return addTrajectory(node.getSe(), trajectory);
        }
        if (envSw.contains(trajEnv)) {
            return addTrajectory(node.getSw(), trajectory);
        }

        System.out.println(trajEnv);

        System.out.println("QuadTrajTree.addTrajectory()");
        Assert.shouldNeverReachHere();

        return null;
    }

    private Envelope getNodeEnvelop(Node node) {
        Envelope nodeEnv = new Envelope();
        nodeEnv.expandToInclude(node.getX(), node.getY());
        nodeEnv.expandToInclude(node.getX() + node.getW(), node.getY() + node.getH());
        return nodeEnv;
    }

    //public Node getTrajQNode(CoordinateArraySequence trajectory) {
        //return intraTrajToNodeMap.get(trajectory);
    //}

    public ArrayList<CoordinateArraySequence> getQNodeTrajs(Node node) {

        ArrayList<CoordinateArraySequence> empty = new ArrayList<CoordinateArraySequence>();
        if (node == null) {
            return empty;
        }
        ArrayList<CoordinateArraySequence> ret = nodeToIntraTrajsMap.get(node);
        return ret == null ? empty : ret;
    }

    public ArrayList<Integer> getQNodeTrajsId(Node node) {

        ArrayList< Integer> empty = new ArrayList<Integer>();
        if (node == null) {
            return empty;
        }
        ArrayList<Integer> ret = nodeToIntraTrajsIdMap.get(node);
        return ret == null ? empty : ret;
    }

    public void draw() {
        QuadTrajTreeCanvas quadTrajTreeCanvas = new QuadTrajTreeCanvas(this);
        quadTrajTreeCanvas.draw();
    }

    public QuadTree getQuadTree() {
        return quadTree;
    }

    boolean containsExtended(Node qNode, Coordinate coord) {
        // checking whether an extended qNode contains a point
        double minX = qNode.getX() - proximity;
        double minY = qNode.getY() - proximity;
        double maxX = minX + qNode.getW() + 2 * proximity;
        double maxY = minY + qNode.getH() + 2 * proximity;
        if (coord.x < minX || coord.y < minY || coord.x > maxX || coord.y > maxY) {
            return false;
        }
        return true;
    }

    public double evaluateNodeTrajWithIndex(Node qNode, ArrayList<CoordinateArraySequence> facilityQuery, HashSet<Integer> served) {
        if (facilityQuery == null || facilityQuery.isEmpty() || qNode == null) {
            return 0;
        }

        if (nodeToIntraTrajQuadTree.get(qNode) == null) {
            return 0;
        }

        ArrayList<CoordinateArraySequence> queryUnionSet = facilityQuery;

        
        for (CoordinateArraySequence coordSeq : queryUnionSet) {
            HashSet<Integer> start = new HashSet<Integer>();
            HashSet<Integer> end = new HashSet<Integer>();
            for (int i = 0; i < coordSeq.size(); i++) {
                double xmin = coordSeq.getX(i) - proximity;
                double ymin = coordSeq.getY(i) - proximity;
                double xmax = coordSeq.getX(i) + proximity;
                double ymax = coordSeq.getY(i) + proximity;

                Point[] points = nodeToIntraTrajQuadTree.get(qNode).searchIntersect(xmin, ymin, xmax, ymax);

                for (Point point : points) {
                    @SuppressWarnings("unchecked")
                    Pair<Integer, Boolean> data = (Pair<Integer, Boolean>) point.getValue();
                    if (data.getValue1()) {
                        start.add(data.getValue0());
                    } else {
                        end.add(data.getValue0());
                    }
                }
            }
            for (Integer id : end) {
                if (start.contains(id)) {
                    served.add(id);
                    //System.out.println(id);
                }
            }
        }

        return served.size();
    }

    public ArrayList<Integer> getQNodeAllTrajsId(Node qNode) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        if (qNode == null) {
            return arrayList;
        }
        if (qNode.getNe() != null) {
            arrayList.addAll(getQNodeTrajsId(qNode.getNe()));
        }
        if (qNode.getNw() != null) {
            arrayList.addAll(getQNodeTrajsId(qNode.getNw()));
        }
        if (qNode.getSe() != null) {
            arrayList.addAll(getQNodeTrajsId(qNode.getSe()));
        }
        if (qNode.getSw() != null) {
            arrayList.addAll(getQNodeTrajsId(qNode.getSw()));
        }

        if (qNode.getNe() != null) {
            arrayList.addAll(getQNodeAllTrajsId(qNode.getNe()));
        }
        if (qNode.getNw() != null) {
            arrayList.addAll(getQNodeAllTrajsId(qNode.getNw()));
        }
        if (qNode.getSe() != null) {
            arrayList.addAll(getQNodeAllTrajsId(qNode.getSe()));
        }
        if (qNode.getSw() != null) {
            arrayList.addAll(getQNodeAllTrajsId(qNode.getSw()));
        }
        return arrayList;
    }

    public ArrayList<CoordinateArraySequence> getQNodeAllTrajs(Node qNode) {
        ArrayList<CoordinateArraySequence> arrayList = new ArrayList<CoordinateArraySequence>();
        if (qNode == null) {
            return arrayList;
        }
        if (qNode.getNe() != null) {
            arrayList.addAll(getQNodeTrajs(qNode.getNe()));
        }
        if (qNode.getNw() != null) {
            arrayList.addAll(getQNodeTrajs(qNode.getNw()));
        }
        if (qNode.getSe() != null) {
            arrayList.addAll(getQNodeTrajs(qNode.getSe()));
        }
        if (qNode.getSw() != null) {
            arrayList.addAll(getQNodeTrajs(qNode.getSw()));
        }

        if (qNode.getNe() != null) {
            arrayList.addAll(getQNodeAllTrajs(qNode.getNe()));
        }
        if (qNode.getNw() != null) {
            arrayList.addAll(getQNodeAllTrajs(qNode.getNw()));
        }
        if (qNode.getSe() != null) {
            arrayList.addAll(getQNodeAllTrajs(qNode.getSe()));
        }
        if (qNode.getSw() != null) {
            arrayList.addAll(getQNodeAllTrajs(qNode.getSw()));
        }
        return arrayList;
    }

    /*
	public double evaluateNodeTrajBruteForce(Node qNode, ArrayList<CoordinateArraySequence> facilityQuery) {
		if (facilityQuery == null || facilityQuery.isEmpty()) {
			return 0;
	    }
	    ArrayList<CoordinateArraySequence> allTrajs = getQNodeAllTrajs(qNode);
	    ArrayList<Integer> allTrajIds = getQNodeAllTrajsId(qNode);
		return calculateCover(allTrajs, allTrajIds, facilityQuery);
	}*/
    public double evaluateNodeTraj(Node qNode, ArrayList<CoordinateArraySequence> facilityQuery) {
        
        if (facilityQuery == null || facilityQuery.isEmpty()) {
            return 0;
        }
        ArrayList<CoordinateArraySequence> interNodeTrajs = getQNodeTrajs(qNode);
        ArrayList<Integer> interNodeTrajsId = getQNodeTrajsId(qNode);
        if (interNodeTrajs == null || interNodeTrajs.isEmpty()) {
            return 0;
        }
        return calculateCover(interNodeTrajs, interNodeTrajsId, facilityQuery);
    }

    public double calculateCover(ArrayList<CoordinateArraySequence> trajs, ArrayList<Integer> trajIds, ArrayList<CoordinateArraySequence> facilityQuery) {
        ArrayList<CoordinateArraySequence> queryUnionSet = facilityQuery;
        // for each connected component, currently each route is considered to be a connected component, may have to improve it
        double serviceValue = 0;
        Set<Integer> alreadyServed = new HashSet<Integer>();
        for (int i = 0; i < queryUnionSet.size(); i++) {
            if (queryUnionSet.get(i).size() < 2) {
                continue;
            }
            HashMap<CoordinateArraySequence, Set<Coordinate>> servedTrajs = new HashMap<CoordinateArraySequence, Set<Coordinate>>();
            // hashset instead of array/arraylist is used considering the general case where one user trajectory can consist of multiple points
            for (int j = 0; j < queryUnionSet.get(i).size(); j++) {
                Coordinate coord = queryUnionSet.get(i).getCoordinate(j);   // coord of a facility point
                // taking each point of facility subgraph we are checking against each point of inter node trajectories
                for (int k = 0; k < trajs.size(); k++) {
                    for (int l = 0; l < trajs.get(k).size(); l++) {
                        Coordinate point = trajs.get(k).getCoordinate(l);  // coord of a user trajectory point
                        // is it ok to calculate euclidean distance? Or should it be a rectange(square) with 2*proximity as side length?
                        double euclideanDistance = Math.sqrt(Math.pow((coord.x - point.x), 2) + Math.pow((coord.y - point.y), 2));
                        if (Math.abs(coord.x - point.x) > proximity || Math.abs(coord.y - point.y) > proximity) {
                            continue;
                        }
                        if (!servedTrajs.containsKey(trajs.get(k))) {
                            servedTrajs.put(trajs.get(k), new HashSet<Coordinate>());
                        }
                        servedTrajs.get(trajs.get(k)).add(point);
                    }
                }
            }
            for (int k = 0; k < trajs.size(); k++) {
                if (!servedTrajs.containsKey(trajs.get(k)) || servedTrajs.get(trajs.get(k)).size() < 2 || alreadyServed.contains(trajIds.get(k))) {
                    continue;
                }
                //System.out.println("<" + interNodeTrajs.get(k).getX(0) + ", " + interNodeTrajs.get(k).getY(0) + ">        <" + interNodeTrajs.get(k).getX(1) + ", " + interNodeTrajs.get(k).getY(1) + ">");
                //System.out.println(trajIds.get(k));
                alreadyServed.add(trajIds.get(k));
                serviceValue += 1;
            }
        }
        return serviceValue;
    }

    public ArrayList<CoordinateArraySequence> makeUnionSet(ArrayList<CoordinateArraySequence> trajectories) {
        // proximity ignored during union
        // each route is considered as a node
        // two nodes are connected by an edge if the corresponding routes share at least one commmon point
        int[] representative = new int[trajectories.size()];
        ArrayList<ArrayList<Integer>> stoppageSharingRoutes = new ArrayList<ArrayList<Integer>>();
        ArrayList<HashSet<Coordinate>> stoppages = new ArrayList<HashSet<Coordinate>>();

        // initialize
        for (int i = 0; i < trajectories.size(); i++) {
            representative[i] = -1;
            stoppageSharingRoutes.add(new ArrayList<Integer>());
            stoppages.add(new HashSet<Coordinate>());
            for (int j = 0; j < trajectories.get(i).size(); j++) {
                Coordinate point = trajectories.get(i).getCoordinate(j);
                stoppages.get(i).add(point);
            }
        }

        // edge detection between nodes
        for (int i = 0; i < trajectories.size(); i++) {
            boolean notFound = true;
            for (int j = 0; j < i && notFound; j++) {
                for (int l = 0; l < trajectories.get(i).size(); l++) {
                    Coordinate point = trajectories.get(i).getCoordinate(l);
                    if (stoppages.get(j).contains(point)) {
                        notFound = false;
                        stoppageSharingRoutes.get(i).add(j);
                        stoppageSharingRoutes.get(j).add(i);
                        break;
                    }
                }
            }
        }

        // CoordinateArraySequence refers to individual routes consisting of many points in trajectories [Assumption: Each route is a connected component]
        // CoordinateArraySequence refers to individual connected components in unionFacilities
        ArrayList<CoordinateArraySequence> unionFacilities = new ArrayList<CoordinateArraySequence>();
        ArrayList<Coordinate> connectedComponent = new ArrayList<Coordinate>();
        // BFS to update connected components
        int curRep = 0;
        for (int i = 0; i < trajectories.size(); i++) {
            if (representative[i] != -1) {
                continue;
            }
            Queue<Integer> nodes = new LinkedList<Integer>();
            nodes.add(i);
            for (int j = 0; j < trajectories.get(i).size(); j++) {
                connectedComponent.add(trajectories.get(i).getCoordinate(j));
            }
            representative[i] = curRep;
            while (!nodes.isEmpty()) {
                int curNode = nodes.peek();
                nodes.remove();
                for (int j = 0; j < stoppageSharingRoutes.get(curNode).size(); j++) {
                    int neighbor = stoppageSharingRoutes.get(curNode).get(j);
                    if (representative[neighbor] == -1) {
                        nodes.add(neighbor);
                        representative[neighbor] = curRep;
                        for (int k = 0; k < trajectories.get(neighbor).size(); k++) {
                            connectedComponent.add(trajectories.get(neighbor).getCoordinate(k));
                        }
                    }
                }
            }
            Coordinate[] connectedComponentArray = new Coordinate[connectedComponent.size()];
            connectedComponent.toArray(connectedComponentArray);
            unionFacilities.add(new CoordinateArraySequence(connectedComponentArray, connectedComponent.size()));
            connectedComponent.clear();
            connectedComponent = new ArrayList<Coordinate>();
            curRep++;
        }
        stoppageSharingRoutes.clear();
        stoppages.clear();
        return unionFacilities;
    }

    public ArrayList<CoordinateArraySequence> clipGraph(Node node, ArrayList<CoordinateArraySequence> facilityQuery) {
        ArrayList<CoordinateArraySequence> clippedSubgraphs = new ArrayList<CoordinateArraySequence>();
        //System.out.println("In clipGraph graph size = " + facilityQuery.size());
        for (int i = 0; i < facilityQuery.size(); i++) {
            ArrayList<Coordinate> route = new ArrayList<Coordinate>();
            for (int j = 0; j < facilityQuery.get(i).size(); j++) {
                //System.out.print(facilityQuery.get(i).getCoordinate(j) + " ");
                if (containsExtended(node, facilityQuery.get(i).getCoordinate(j))) {
                    route.add(new Coordinate(facilityQuery.get(i).getCoordinate(j).x, facilityQuery.get(i).getCoordinate(j).y));
                }
            }
            if (route.size() <= 1) {
                continue; // can't serve
            }
            Coordinate[] routePointsArray = new Coordinate[route.size()];
            route.toArray(routePointsArray);
            clippedSubgraphs.add(new CoordinateArraySequence(routePointsArray, route.size()));
        }
        return clippedSubgraphs;
    }

    public int getTotalNodeTraj(Node qNode) {
        if (nodeToAllTrajsCount.get(qNode) != null) {
            return nodeToAllTrajsCount.get(qNode);
        }
        return 0;
    }

}
